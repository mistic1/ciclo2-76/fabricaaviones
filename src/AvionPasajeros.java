public class AvionPasajeros extends Avion{
    /***
     * atributos
     ***/
    private int pasajeros;

    /****************
     * Constructores
     ****************/
    public AvionPasajeros(String color, double tamaño, int pasajeros){
        super(color, tamaño);
        this.pasajeros = pasajeros;
    }

    /************
     * Métodos
     ***********/
    public void servir(){
        for (int i=1;i<=pasajeros;i++){
        System.out.println("Atendiendo al pasajero- "+i);
        }
    }
}

